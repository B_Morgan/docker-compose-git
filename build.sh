#!/bin/sh

echo "Building image"
docker-compose build

echo "create container"
docker-compose up -d 

echo "entering jenkins container"
docker exec -it dockercompose_jenkins_1 docker version